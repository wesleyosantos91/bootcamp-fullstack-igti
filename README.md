# BOOTCAMP ONLINE IGTI

### Desenvolvedor Full Stack

> O Bootcamp tem como objetivo oferecer uma formação prática e intensiva que habilite o profissional para atuar como desenvolvedor Full Stack.

# Tecnologias

- BACKEND
    - Javascript
    - NodeJS
- FRONTEND
    - HTML
    - CSS
    - REACTJS
- BANCO DE DADOS
    MongoDB
- DEVOPS
    - GIT
    - Heroku
- FERRAMENTAS
    - VSCode
